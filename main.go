package main

import (
	"code.google.com/p/goauth2/oauth"
	"code.google.com/p/google-api-go-client/drive/v2"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"
)

//HTML template for login
var loginTemplate = template.Must(template.New("").Parse(`
<html><body>
<b>Please authenticate this app with the Google OAuth provider.</b>
<br/>
<form action="/login" method="POST"><input type="submit" value="Ok, authorize this app with my id"/></form>
</body></html>
`))

// Settings for authorization.
var config = &oauth.Config{
	// upadte following with your CLIENT_ID, CLIENT_SECRET, CLIENT_CALLBACK_URL
	ClientId:     "CLIENT_ID",
	ClientSecret: "CLIENT_SECRET",
	RedirectURL:  "CLIENT_CALLBACK_URL",
	// don't change any of these
	Scope:    "https://www.googleapis.com/auth/drive",
	AuthURL:  "https://accounts.google.com/o/oauth2/auth",
	TokenURL: "https://accounts.google.com/o/oauth2/token",
}

//response
type Response struct {
	AccessToken  string    `json:"access_token"`
	RefreshToken string    `json:"refresh_token,omitempty"`
	Expiry       time.Time `json:"expiry,omitempty"`
	Files        []string  `json:"fiels,omitempty"`
}

// Setup route for app urls
func main() {
	http.HandleFunc("/", handleIndex)
	http.HandleFunc("/login", handleLogin)
	http.HandleFunc("/oauth2callback", handleCallback)
	log.Fatal(http.ListenAndServe(":7979", nil))
}

// **************** request handlers

// show login page
func handleIndex(w http.ResponseWriter, r *http.Request) {
	loginTemplate.Execute(w, nil)
}

// redirect to google auth service
func handleLogin(w http.ResponseWriter, r *http.Request) {
	authUrl := config.AuthCodeURL("state")
	http.Redirect(w, r, authUrl, http.StatusFound)
}

// Oauth callback handler, fetch acess token for the user
// create service for google drive and fetch users list of file
// response jeson returns -
// Access token, Refresh token, Time limit, List of  File paths
func handleCallback(w http.ResponseWriter, r *http.Request) {
	code := r.FormValue("code")
	t := &oauth.Transport{
		Config:    config,
		Transport: http.DefaultTransport,
	}
	token, err := t.Exchange(code)
	if err != nil {
		log.Fatalf("An error occurred during token exchange: %v\n", err)
	}

	svc, err := drive.New(t.Client())
	if err != nil {
		log.Fatalf("An error occurred creating Drive client: %v\n", err)
	}

	f, err := allFiles(svc)
	if err != nil {
		log.Fatalf("An error occurred during retiving file list from Drive: %v\n", err)
	}
	response := Response{token.AccessToken, token.RefreshToken, token.Expiry, make([]string, 0)}
	for _, v := range f {
		response.Files = append(response.Files, v.AlternateLink)
	}
	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode(response)
}

// **************** helper functions
//fetches and displays all files
func allFiles(d *drive.Service) ([]*drive.File, error) {
	var fs []*drive.File
	pageToken := ""
	for {
		q := d.Files.List()
		// If we have a pageToken set, apply it to the query
		if pageToken != "" {
			q = q.PageToken(pageToken)
		}
		r, err := q.Do()
		if err != nil {
			fmt.Printf("An error occurred: %v\n", err)
			return fs, err
		}
		fs = append(fs, r.Items...)
		pageToken = r.NextPageToken
		if pageToken == "" {
			break
		}
	}
	return fs, nil
}
